use strict;
use warnings;

use Test::More;
use Test::Command::Simple;

use File::chdir;
use Path::Tiny;

# support overriding to instead test command installed into system
my @command
	= ($ENV{DISTINKT_DIST}) || ( 'perl', '../../script/distinkt-dist' );

my $sourcedir = path('examples/p5-acme-example-dist');

{
	local $CWD = $sourcedir;
	run_ok @command;
}
cmp_ok stdout, 'eq', '', 'nothing printed on stdout';
unlike stderr,
	qr{^(?!(?:Processing|Finding|Distribution seems suitable|Copying|Writing) )}ms,
	'only harmless looking line intros in stderr';
unlike stderr,
	qr/^No such file in archive:/m,
	'no complaint about missing file';
ok $sourcedir->child('Acme-Example-Dist-0.001.tar.gz')->remove,
	'remove created tarball';

done_testing;
